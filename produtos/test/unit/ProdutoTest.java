package unit;

import org.junit.BeforeClass;
import org.junit.Test;

import builders.ProdutoBuilder;
import models.Produto;
import play.test.UnitTest;
import utils.DatabaseCleaner;

public class ProdutoTest extends UnitTest {

	@BeforeClass
	public static void setUp() {
		
		new DatabaseCleaner().cleanUp();
		
	}
	
	@Test
	public void deveCriarProdutoComNome() {
		
		String nome = "Produto teste";
		Produto p = new ProdutoBuilder().comNome(nome).build();
		
		assertNotNull("Referencia de produto nula.", p);
		assertNotNull("Referencia de atributo 'nome' nula. Esperado '"+ nome +"'.", p.nome);
		assertEquals("Valor inexperado", nome, p.nome);
	}
	
}