package builders;

import java.math.BigDecimal;

import models.Produto;

public class ProdutoBuilder extends BaseModelBuilder<Produto, Long> {

	public ProdutoBuilder() {
		super(new Produto());
	}
	
	public ProdutoBuilder(Produto model) {
		super(model);
	}

	@Override
	protected BaseModelBuilder<Produto, Long> padrao() {
		
		this.model.nome = "Produto padrão";
		this.model.preco = new BigDecimal(20.00);
		this.model.quantidadeEmEstoque = 20;
		
		return this;
	}

	public ProdutoBuilder comNome(String nome) {
		
		this.model.nome = nome;
		return this;
		
	}
	
}
