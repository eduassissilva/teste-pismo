package builders;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import play.db.jpa.GenericModel;
import play.db.jpa.JPA;

public abstract class BaseModelBuilder<T extends GenericModel, I extends Number> {

	protected T model;
	
	private static Map<Class, Number> idsPorModel = new HashMap<Class, Number>();
	
	private Class<T> modelClass = getModelClass();
	
	protected abstract BaseModelBuilder<T, I> padrao();
	
	public BaseModelBuilder(T model) {
		this.model = model;
	}
	
	public T build() {
		return this.model;
	}
	
	public T save() {
		T model = build();
		
		if(model == null) {
			throw new RuntimeException("Para utilizar este método deve-se implementar o método 'build'.");
		}
		
		model._save();
		
		return model;
	}
	
	private Class<T> getModelClass() {

		Type genericSuperclass = this.getClass().getGenericSuperclass();

		if (genericSuperclass instanceof ParameterizedType) {

			ParameterizedType pt = (ParameterizedType) genericSuperclass;
			Type type = pt.getActualTypeArguments()[0];

			return (Class<T>) type;
		}

		throw new IllegalStateException("O builder deve extender a class GenericModel<T> passando a classe T, que é o tipo da model.");
	}
	
	protected I gerarId() {
		if (!idsPorModel.containsKey(this.modelClass)) {
			configurarPrimeiroId();
		}
		
		Number id = idsPorModel.get(this.modelClass);
		id = id.intValue() + 1;
		idsPorModel.put(this.modelClass, id);
		
		return (I) id;
	}
	
	private void configurarPrimeiroId() {

		String jpql = "select m.id from " + getModelClass().getName() + " m order by id desc";
		System.out.println(jpql);

		List<I> ids = JPA.em().createQuery(jpql).setMaxResults(1).getResultList();

		if (ids == null || ids.isEmpty() || ids.get(0) == null) {
			idsPorModel.put(modelClass, 0);
		} else {
			idsPorModel.put(modelClass, ids.get(0));
		}
	}

}
