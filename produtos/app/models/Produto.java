package models;

import play.db.jpa.GenericModel;
import play.db.jpa.Model;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "produto")
public class Produto extends GenericModel {

	@Id
	public Integer id;
	
	public String nome;
	
	public BigDecimal preco;
	
	public Integer quantidadeEmEstoque;
	
}
